package com.bigfans.cartservice.service.impl;

import com.bigfans.cartservice.dao.ProductDAO;
import com.bigfans.cartservice.dao.ProductSpecDAO;
import com.bigfans.cartservice.model.Product;
import com.bigfans.cartservice.model.ProductSpec;
import com.bigfans.cartservice.service.ProductService;
import com.bigfans.framework.dao.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 商品服务类
 * 
 * @author lichong
 *
 */
@Service(ProductServiceImpl.BEAN_NAME)
public class ProductServiceImpl extends BaseServiceImpl<Product> implements ProductService {
	
	public static final String BEAN_NAME = "productService";
	
	@Autowired
	private ProductSpecDAO productSpecDAO;

	private ProductDAO productDAO;
	
	@Autowired
	public ProductServiceImpl(ProductDAO productDAO) {
		super(productDAO);
		this.productDAO = productDAO;
	}

	@Transactional(readOnly = true)
	public Product getDetailById(String pid) throws Exception {
		Product product = super.load(pid);
		// 规格信息
		List<ProductSpec> specs = productSpecDAO.listByProdId(pid);
		product.setSpecList(specs);
		return product;
	}
}
