package com.bigfans.cartservice.model;

import com.bigfans.cartservice.model.entity.CartItemEntity;
import com.bigfans.framework.utils.ArithUtils;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * 
 * @Description:购物车
 * @author lichong 
 * 2014年12月11日下午11:12:35
 *
 */
@Data
public class CartItem extends CartItemEntity {
	
	private static final long serialVersionUID = -2719512355397557769L;
	
	private String prodName;
	private BigDecimal weight;
	private BigDecimal originalPrice = BigDecimal.ZERO;
	private BigDecimal price = BigDecimal.ZERO;
	private String prodImg;
	private boolean productOnSale = true;
	private BigDecimal subTotal = BigDecimal.ZERO;
	private BigDecimal originalSubTotal = BigDecimal.ZERO;
	private boolean subTotalDiscounted;
	private boolean priceDiscounted;
	private List<Promotion> promotionList;
	private List<Coupon> couponList;
	private List<ProductSpec> specList;
	
	private BigDecimal dlyFee;
	
	public BigDecimal getOriginalSubTotal(){
		if(originalSubTotal == null){
			originalSubTotal = ArithUtils.mul(originalPrice, quantity);
		}
		return originalSubTotal;
	}
	
	public BigDecimal getSavedMoney(){
		return ArithUtils.sub(getOriginalSubTotal(), subTotal);
	}
	
	public void setSubTotal(BigDecimal subTotal) {
		this.subTotal = subTotal;
		this.subTotalDiscounted = true;
	}
	
	public BigDecimal getCurrentPrice(){
		return isSubTotalDiscounted() ? getPrice() : getOriginalPrice();
	}
	
	public BigDecimal getCurrentSubTotal(){
		return isSubTotalDiscounted() ? getSubTotal() : getOriginalSubTotal();
	}
	
	public void setPrice(BigDecimal price) {
		this.price = price;
		this.priceDiscounted = true;
	}

}
