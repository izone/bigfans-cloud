package com.bigfans.cartservice.model.entity;

import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

@Data
@Table(name="Product_Spec")
public class ProductSpecEntity extends AbstractModel {

	private static final long serialVersionUID = -6811834593128832769L;

	@Column(name="prod_id")
	protected String prodId;
	@Column(name="option_id")
	protected String optionId;
	@Column(name="option")
	protected String option;
	@Column(name="value_id")
	protected String valueId;
	@Column(name="value")
	protected String value;

	@Override
	public String getModule() {
		return "ProductSpec";
	}

}
