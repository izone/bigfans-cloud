package com.bigfans.searchservice;

import com.bigfans.Constants;
import com.bigfans.framework.BeanProviderSpring;
import com.bigfans.framework.CurrentUser;
import com.bigfans.framework.CurrentUserFactory;
import com.bigfans.framework.kafka.KafkaConsumerBean;
import com.bigfans.framework.kafka.KafkaConsumerTaskManager;
import com.bigfans.searchservice.api.auth.SearchServiceFunctionalUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class StartupListener implements CommandLineRunner {

	@Autowired
	private KafkaConsumerTaskManager consumerTaskManager;

	@Autowired
	private ApplicationContext applicationContext;

	@Override
	public void run(String... arg0) throws Exception {
		Map<String, Object> beansWithAnnotation = this.applicationContext.getBeansWithAnnotation(KafkaConsumerBean.class);
		consumerTaskManager.registerListeners(beansWithAnnotation.values().iterator());
		consumerTaskManager.consume();

		BeanProviderSpring.initContext(applicationContext);

		CurrentUser functionalUser = new SearchServiceFunctionalUser();
		String functionalUserToken = CurrentUserFactory.createToken(functionalUser, Constants.TOKEN.JWT_SECURITY_KEY);
		SearchApplications.setFunctionalUser(functionalUser);
		SearchApplications.setFunctionalUserToken(functionalUserToken);
	}
}
