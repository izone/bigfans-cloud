package com.bigfans.searchservice.service;

import com.bigfans.framework.es.request.SearchResult;
import com.bigfans.searchservice.model.Product;

public interface RecommendService {

    SearchResult<Product> moreLikeThis(String prodId , Integer limit);
}
