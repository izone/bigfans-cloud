package com.bigfans.shippingservice.model.entity;

import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * @Description:配送实体
 * @author lichong 2014年12月14日下午4:48:01
 *
 */
@Data
@Table(name="Delivery")
public class DeliveryEntity extends AbstractModel {
	private static final long serialVersionUID = 5625942998949303053L;
	
	public static final String STATUS_UNSENT = "preparing";
	public static final String STATUS_DELIVERING = "delivering";
	public static final String STATUS_SIGNED = "signed";

	@Column(name="order_id")
	protected String orderId;
	@Column(name="user_id")
	protected String userId;
	@Column(name="address")
	protected String address;
	@Column(name="consignee")
	protected String consignee;
	@Column(name="postalcode")
	protected String postalcode;
	@Column(name="mobile")
	protected String mobile;
	@Column(name="tel")
	protected String tel;
	@Column(name="email")
	protected String email;
	@Column(name="complete_date",columnDefinition="DATETIME")
	protected Date completeDate;
	@Column(name="status")
	protected String status = STATUS_UNSENT;
	// 运费
	@Column(name="freight")
	protected BigDecimal freight;
	
	public String getModule() {
		return "Delivery";
	}

}