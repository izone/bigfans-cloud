package org.bigfans.cloud.api.gateway.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author lichong
 * @create 2018-07-29 下午7:46
 **/
@Configuration
public class RouteConfig {

    public RouteLocator customRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(t -> t.path("/hello")
                        .and()
                        .uri("http://localhost:8080"))
                .build();
    }

}
