package com.bigfans.paymentservice.config;

import com.bigfans.framework.cache.CacheProvider;
import com.bigfans.framework.cache.RedisCacheProvider;
import com.bigfans.framework.plugins.*;
import com.bigfans.framework.redis.JedisTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author lichong
 * @create 2018-03-21 下午9:21
 **/
@Configuration
@RefreshScope
public class PluginConfig {

    @Value("${qiniu.accessKey}")
    private String qiniuAccessKey;
    @Value("${qiniu.secretKey}")
    private String qiniuSecretKey;
    @Value("${qiniu.bucketName}")
    private String qiniuBucketName;
    @Value("${qiniu.bucketHostName}")
    private String qiniuBucketHostName;

    @Value("${cache.type}")
    private String cacheType;
    @Value("${cache.dbindex}")
    private Integer cacheDbIndex;

    @Bean
    public FileStoragePlugin fileStoragePlugin(){
        return new LocalStoragePlugin();
    }

    @Bean
    public CacheProvider cachePlugin(JedisTemplate jedisTemplate){
        CacheProvider cacheProvider = new RedisCacheProvider(jedisTemplate , cacheDbIndex);
        return cacheProvider;
    }
}
