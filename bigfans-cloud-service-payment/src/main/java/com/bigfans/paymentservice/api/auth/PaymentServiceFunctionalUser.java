package com.bigfans.paymentservice.api.auth;

import com.bigfans.framework.CurrentUser;

import java.util.Arrays;
import java.util.Date;

/**
 * @author lichong
 * @create 2018-04-27 下午8:47
 **/
public class PaymentServiceFunctionalUser extends CurrentUser {

    public PaymentServiceFunctionalUser() {
        this.setAccount("payment-service-functional-id");
        this.setLoggedIn(true);
        this.setType(TYPE.FUNCTIONAL_ID);
        this.setPeriod(-1);
        this.setLoginTime(new Date());
        this.setPermissions(Arrays.asList("all"));
        this.setRoles(Arrays.asList("admin"));
    }

}
