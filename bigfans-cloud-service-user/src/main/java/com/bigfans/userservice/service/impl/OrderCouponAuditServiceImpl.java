package com.bigfans.userservice.service.impl;

import com.bigfans.framework.dao.BaseServiceImpl;
import com.bigfans.userservice.dao.OrderCouponAuditDAO;
import com.bigfans.userservice.model.OrderCouponAudit;
import com.bigfans.userservice.service.OrderCouponAuditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(OrderCouponAuditServiceImpl.BEAN_NAME)
public class OrderCouponAuditServiceImpl extends BaseServiceImpl<OrderCouponAudit> implements OrderCouponAuditService {

    public static final String BEAN_NAME = "userCouponLogServiceImpl";

    private OrderCouponAuditDAO orderCouponAuditDAO;

    @Autowired
    public OrderCouponAuditServiceImpl(OrderCouponAuditDAO orderCouponAuditDAO) {
        super(orderCouponAuditDAO);
        this.orderCouponAuditDAO = orderCouponAuditDAO;
    }

    @Override
    public OrderCouponAudit getByOrder(String orderId) throws Exception {
        return orderCouponAuditDAO.getByOrder(orderId);
    }
}
