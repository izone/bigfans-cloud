package com.bigfans.userservice.api.form;

import lombok.Data;

@Data
public class UserRegisterForm {

    /** 注册表单数据 */
    private String mobile;
    private String email;
    private String password;
    private String confirmedPassword;
    private String verificationCode;
    private Boolean agreeTerms;

}
