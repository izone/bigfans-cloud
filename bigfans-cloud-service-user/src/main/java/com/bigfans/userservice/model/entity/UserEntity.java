package com.bigfans.userservice.model.entity;

import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;
import java.util.Date;

@Data
@Table(name="User")
public class UserEntity extends AbstractModel {

	public String getModule() {
		return "User";
	}
	
	private static final long serialVersionUID = -1189566871934067878L;

	public static final int STATUS_ACTIVATED = 1;
	public static final int STATUS_NOT_ACTIVATED = 0;

	public static final String COL_ACCOUNT = "account";
	public static final String COL_PASSWORD = "password";
	public static final String COL_EMAIL = "email";
	public static final int SEX_MAN = 1;
	public static final int SEX_WOMAN = 0;

	@Column(name = "active_key")
	protected String activeKey;
	@Column(name = "status")
	protected Integer status;
	@Column(name = "account")
	protected String account;
	@Column(name = "password")
	protected String password;
	@Column(name = "email")
	protected String email;
	@Column(name = "mobile")
	protected String mobile;
	@Column(name = "platform")
	protected String platform;
	@Column(name = "reg_date",nullable=false,columnDefinition=COLUMN_TYPE_DATETIME)
	protected Date regDate;
	@Column(name = "reg_type")
	protected String regType;
	@Column(name = "nickname")
	protected String nickname;
	@Column(name = "age")
	protected Integer age;
	@Column(name = "sex")
	protected Integer sex;
	@Column(name = "icon_path")
	protected String iconPath;
	@Column(name = "points")
	protected Float points;

	@Override
	public String toString() {
		return "User [activeKey=" + activeKey + ", status=" + status + ", account=" + account + ", password="
				+ password + ", email=" + email + ", mobile=" + mobile + ", regDate=" + regDate + ", regType="
				+ regType + ", nickname=" + nickname + ", age=" + age + ", sex=" + sex + ", iconPath=" + iconPath + "]";
	}

}