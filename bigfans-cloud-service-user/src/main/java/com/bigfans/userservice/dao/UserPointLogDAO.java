package com.bigfans.userservice.dao;

import com.bigfans.framework.dao.BaseDAO;
import com.bigfans.userservice.model.UserPointLog;

public interface UserPointLogDAO extends BaseDAO<UserPointLog> {
}
