package com.bigfans.notificationservice.config;

import com.bigfans.framework.cache.CacheProvider;
import com.bigfans.framework.cache.RedisCacheProvider;
import com.bigfans.framework.plugins.AlidayuSmsPlugin;
import com.bigfans.framework.plugins.SmsPlugin;
import com.bigfans.framework.redis.JedisTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author lichong
 * @create 2018-03-21 下午9:21
 **/
@Configuration
@RefreshScope
public class PluginConfig {

    @Value("${cache.type}")
    private String cacheType;
    @Value("${cache.dbindex}")
    private Integer cacheDbIndex;

    @Value("${alidy.url}")
    private String alidyUrl;
    @Value("${alidy.appKey}")
    private String alidyAppKey;
    @Value("${alidy.secret}")
    private String alidySecret;
    @Value("${alidy.signName}")
    private String alidySignName;
    @Value("${alidy.template.validCode}")
    private String alidyTemplateValidCode;
    @Value("${alidy.template.prodArrival}")
    private String alidyTemplateProdArrival;
    @Value("${alidy.template.priceDown}")
    private String alidyTemplatePriceDown;

    @Bean
    public CacheProvider cachePlugin(JedisTemplate jedisTemplate) {
        CacheProvider cacheProvider = new RedisCacheProvider(jedisTemplate, cacheDbIndex);
        return cacheProvider;
    }

    @Bean
    public SmsPlugin smsPlugin() {
        AlidayuSmsPlugin smsPlugin = new AlidayuSmsPlugin(alidyUrl, alidyAppKey, alidySecret, alidySignName);
        smsPlugin.setTemplate_priceDown(alidyTemplatePriceDown);
        smsPlugin.setTemplate_prodArrival(alidyTemplateProdArrival);
        smsPlugin.setTemplate_valideCode(alidyTemplateValidCode);
        return smsPlugin;
    }
}
