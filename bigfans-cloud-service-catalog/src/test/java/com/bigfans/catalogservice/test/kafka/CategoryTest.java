package com.bigfans.catalogservice.test.kafka;

import com.bigfans.catalogservice.CatalogServiceApp;
import com.bigfans.catalogservice.model.Category;
import com.bigfans.catalogservice.service.category.CategoryService;
import com.bigfans.framework.exception.ServiceRuntimeException;
import com.bigfans.framework.kafka.KafkaTemplate;
import com.bigfans.model.event.CategoryCreatedEvent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author lichong
 * @create 2018-03-18 下午3:17
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CatalogServiceApp.class)
public class CategoryTest {

    @Autowired
    private KafkaTemplate kafkaTemplate;
    @Autowired
    private CategoryService categoryService;

    @Test
    public void test() throws Exception {
        List<Category> navigatorTree = categoryService.listByLevel(3 , true);
        for (Category cat: navigatorTree) {
            System.out.println(cat.getId());
            kafkaTemplate.send(new CategoryCreatedEvent(cat.getId()));
        }
    }

    @Test
    public void testSend(){
        kafkaTemplate.send(new CategoryCreatedEvent("1001"));
    }


}
